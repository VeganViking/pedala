//creo constante para recojer nodos
// const form$$ = document.querySelector('[data-function=form]');
const name$$ = document.querySelector('[data-function = form_name]');
const phone$$ = document.querySelector('[data-function = form_phone]');
const address$$ = document.querySelector('[data-function = form_address]');
const email$$ = document.querySelector('[data-function = form_email]')
const submit$$ = document.querySelector('[data-function = form_submit]');

//creo las funciones
function nameValidate() {
    const name = name$$.value;
    if (name !== "") {
        return true;
    }else {
        alert('El campo nombre no puede estar vacio');
        return false;
    }
}

function phoneValidate() {
    const phone = phone$$.value;
    if (phone !== "") {
        return true;
    }else {
        alert('El campo teléfono no puede estar vacio');
        return false;
    }

}

function addressValidate() {
    const address = address$$.value;
    if (address !== "") {
        return true;
    }else {
        alert('El campo dirección no puede estar vacio');
        return false;
    }
}

function emailValidate() {
    const email = email$$.value;
    if (email !== "") {
        return true;
    }else {
        alert('El campo email no puede estar vacio');
        return false;
    }
}


function allValidates() {
    if (nameValidate() && phoneValidate() && addressValidate() && emailValidate()) {
        alert('OK');
    }else {
        alert('error');
    }
}


//ponemos addeventlistener
submit$$.addEventListener('click', allValidates);