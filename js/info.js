//BASE DE DATOS

const models = [

{
    code : '001',
    name: 'BICICLETA SCOTT PLASMA PREMIUM',
    frame : 'Carbon HMX Plasma 6 Disc',
    gear : 'SRAM RED eTap AXS, 24 velocidades',
    tires: 'Llanta tipo clincher de carbono Zipp 808 NSW',
    wheel : 'Cubiertas Schwalbe PRO ONE TT TL',
    components: 'Componentes de carbono Syncros',
    price : '14.999 €',
    img : "./img/scott_cycles/road/SCOTT_PLASMA_PREMIUM.png"
},

{
    code : '002',
    name: 'BICICLETA SCOTT ADDICT 30 DISC',
    frame : 'Cuadro de carbono HMF Addict Disc',
    gear : 'Shimano Tiagra Disc, 20 velocidades',
    tires:  'Syncros Race 24 Disc',
    wheel : 'Cubiertas Schwalbe ONE',
    components: 'Componentes de carbono/aluminio Syncros',
    price : '1.899 €',
    img: "./img/scott_cycles/road/scott_addict_30.jpeg"
},

{
    code : '003',
    name: 'BICICLETA CANNONDALE ULTEGRA Di12',
    frame : 'Cuadro de Carbono Hi-MOD',
    gear : 'Shimano HG701, 11-speed, Shimano Ultegra Di2, 11-speed',
    tires : 'HollowGram 64 Knot, carbono',
    wheel : 'Vittoria Rubino Pro Speed, 700 x 25c',
    components: 'Componentes de Carbono HollowGram',
    price : '8,399 €',
    img: "./img/cannondale_cycles/road/C21_C11301M_SystemSix_HM_Ult_REP_PD.png"
},

{
    code : '004',
    name: 'BICICLETA CANNONDALE SUPERX 1',
    frame : 'Superlight, durable BallisTec Carbon frameset',
    gear : 'Shimano GRX 800 11-speed hydraulic disc group',
    tires : 'CX 1.0, 28h, Stainless Steel, 14g',
    wheel : 'WTB Nano TCS Light tires, 700 x 40c, tubeless ready',
    components: 'Componentes Cannondale 2 Carbono',
    price : '4,399 €',
    img: "./img/cannondale_cycles/road/C21_C17201M_SuperX_1_SBR_PD.png"
},


{
    code : '005',
    name: 'BICICLETA SCOTT SPARK RC 900 SL AXS',
    frame : 'Cuadro Spark RC en carbono HMX SL de 1799 g',
    gear : 'SRAM XX1 AXS Eagle, Potencímetro',
    tires : 'Ruedas Syncros Silverton 1.0 SL CF',
    wheel : 'Cubiertas Maxxis Rekon Race Folding',
    components: 'Componentes de carbono Syncros SL',
    price : '12.999 €',
    img: "./img/scott_cycles/montain/scott_spark_rc900_axs.jpg"
},


{
    code : '006',
    name: 'BICICLETA SCOTT SPARK RC 900 WORLD CUP',
    frame : 'Cuadro Spark RC de carbono HMX',
    gear : 'SRAM X01 Eagle, 12 velocidades',
    tires : 'Carbono Syncros Silverton 1.0',
    wheel : 'Cubiertas Maxxis Rekon Race Folding',
    components: 'Componentes de carbono Syncros',
    price : '6.649 €',
    img: "./img/scott_cycles/montain/scott_spark_rc900_worldcup.jpg"
},


{
    code : '007',
    name: 'BICICLETA CANNONDALE HABIT CARBON 3',
    frame : 'Cuadro BallisTec Carbon front triangl',
    gear : 'SRAM GX Eagle',
    tires : 'Stan ARCH S1',
    wheel : 'Maxxis Minion DHF 29 x 2.5',
    components: 'Cannondale 3, aleación 6061 forjada en 3D',
    price : '3,799 €',
    img: "./img/cannondale_cycles/montain/C20_C23300M_Habit_Crb_3_EMR_PD.png"
},


{
    code : '008',
    name: 'BICICLETA CANNONDALE F-SI CARBON 4',
    frame : 'BallisTec Carbon, SAVE, PF30-83',
    gear : 'SRAM NX Eagle',
    tires : 'WTB STX i23 TCS, 32h, tubeless ready',
    wheel : 'Schwalbe Racing Ray Performance, 29 x 2.25',
    components: 'CANNONDALE C3',
    price : '3,199 €',
    img: "./img/cannondale_cycles/montain/C21_C25501M_F-Si_Crb_4_ALP_PD.png"
}


];


