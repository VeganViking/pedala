//recojermos los nodos 
const allDivMain$$ = document.querySelectorAll('main > [data-function]');
const h2Home$$ = document.querySelector('header [data-function = h2_home')
const lihome$$ = document.querySelector('header [data-function =li_home]');
const libicicletas$$ = document.querySelector('header [data-function =li_bicicletas]');
const licontacto$$ = document.querySelector('header [data-function =li_contacto]');
const lisobremi$$ = document.querySelector('header [data-function =li_sobremi]');


//declaramos funciones
function showHome() {
    for ( let i = 0; i< allDivMain$$.length; i++) {
        const divIterada$$ = allDivMain$$[i];
        if (divIterada$$.getAttribute('data-function') == "fn_index") {
            divIterada$$.style.display = "block";
        } else {
            divIterada$$.style.display = "none";
        }
    }
}

function showBicicletas() {
    for ( let i = 0; i< allDivMain$$.length; i++) {
        const divIterada$$ = allDivMain$$[i];
        if (divIterada$$.getAttribute('data-function') == "fn_list") {
            divIterada$$.style.display = "block";
        } else {
            divIterada$$.style.display = "none";
        }
    }
}

function showContacto() {
    for ( let i = 0; i< allDivMain$$.length; i++) {
        const divIterada$$ = allDivMain$$[i];
        if (divIterada$$.getAttribute('data-function') == "fn_add") {
            divIterada$$.style.display = "block";
        } else {
            divIterada$$.style.display = "none";
        }
    }
}

function showSobremi() {
    for ( let i = 0; i< allDivMain$$.length; i++) {
        const divIterada$$ = allDivMain$$[i];
        if (divIterada$$.getAttribute('data-function') == "fn_about") {
            divIterada$$.style.display = "block";
        } else {
            divIterada$$.style.display = "none";
        }
    }
}


//ponemos addeventlisteneer
h2Home$$.addEventListener('click', showHome);
lihome$$.addEventListener('click', showHome);
libicicletas$$.addEventListener('click', showBicicletas);
licontacto$$.addEventListener('click', showContacto);
lisobremi$$.addEventListener('click', showSobremi);








