//RECOJO EL NODO DONDE MONTAREMOS TODOS LOS PRODUCTOS
const container$$ = document.querySelector('[data-function="main_list_container"]');


// RECORRER BUCLE

for ( let i = 0; i < models.length; i++) {
    //RECORREMOS EL ARRAY DE OBJETOS(BASE DE DATOS)
    const model = models[i];
    const {name, img, code} = model; //DECONSTRUCTURING PARA SACAR LOS VALORES QUE TIENEN

    //CREAMOS DIV/P/CREAMOS EL ATRIBUTO CON EL CODE DE INFO

    const div$$ = document.createElement('div');
    const p$$ = document.createElement('p');
    p$$.textContent = name;
    div$$.setAttribute("data-function", code);


    div$$.appendChild(p$$);

    //CREAMOS NODO PARA RECOJER LA INFO Y CREAMOS ATRIBUTOS DATA-FUNCTION PARA CLICAR EN EL BOTON Y QUE ALMACENAMOS

    const img$$ = document.createElement('img');
    img$$.setAttribute("src", img);
    img$$.setAttribute("data-function", code);
    p$$.setAttribute("data-function", code);
    div$$.appendChild(img$$);

    //METEMOS DENTRO CONTAINER Y ADDEVENTLISTENER DE CLICK

    div$$.classList.add("main_list_container_product");
    container$$.appendChild(div$$);
    div$$.addEventListener('click', fnAbrirDetail);
}

