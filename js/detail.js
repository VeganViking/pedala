
const box$$ = document.querySelector('[data-function="main_detail_container"]');


function fnAbrirDetail(event) {   
    box$$.innerHTML="";
    //ABRIR PAGINA
    const allDivMain$$ = document.querySelectorAll('main > [data-function]');
    // const detail$$ = document.querySelector('main [data-function=fn_detail]');

    //RECORREMOS DIVS PRINCIPALES Y ESCONDEMOS LOS QUE NO QUEREMOS 

    for ( let i = 0; i <allDivMain$$.length; i++) {
        const div$$ = allDivMain$$[i];
        if (div$$.getAttribute('data-function') == "fn_detail") {
            div$$.style.display = "block";
        } else {
            div$$.style.display = "none";
        }
    }
    // MONTAR PAGINA
    const code = event.target.getAttribute('data-function');
   
    for ( let i = 0; i < models.length; i++) {
        const model = models[i];
        if (model.code == code) {
            const {name, frame, gear, tires, wheel, components, price, img} = model;
            const div$$ = document.createElement('div');
            const name$$ = document.createElement('p');
            const frame$$ = document.createElement('p');
            const gear$$ = document.createElement('p');
            const tires$$ = document.createElement('p');
            const wheel$$ = document.createElement('p');
            const components$$ = document.createElement('p');
            const price$$ = document.createElement('p');
            name$$.textContent = name;   
            frame$$.textContent = frame;
            gear$$.textContent = gear;
            tires$$.textContent = tires;
            wheel$$.textContent = wheel;
            components$$.textContent = components;
            price$$.textContent = price;
            
            //METO LAS CARACTERISTICAS EN EL DIV
        
            const img$$ = document.createElement('img');
            img$$.setAttribute("src", img);
            div$$.appendChild(name$$);
            div$$.appendChild(img$$);
            div$$.appendChild(frame$$);
            div$$.appendChild(gear$$);
            div$$.appendChild(tires$$);
            div$$.appendChild(wheel$$);
            div$$.appendChild(components$$);
            div$$.appendChild(price$$);

            //PONGO CLASE AL DIV Y LO METEMOS EN BOX

            div$$.classList.add("main_detail_container_product");
            name$$.classList.add("main_detail_container_product_name")
            img$$.classList.add("main_detail_container_product_img")
            frame$$.classList.add("main_detail_container_product_frame")
            gear$$.classList.add("main_detail_container_product_gear")//
            tires$$.classList.add("main_detail_container_product_tires")
            wheel$$.classList.add("main_detail_container_product_wheel")
            components$$.classList.add("main_detail_container_product_components")
            price$$.classList.add("main_detail_container_product_price")
            box$$.appendChild(div$$);
            
        }
    }
}

//BOTÓN BACK

function fnAtrasDetail(event) { 
    const allDivMain$$ = document.querySelectorAll('main > [data-function]');
    for ( let i = 0; i <allDivMain$$.length; i++) {
        const divMain$$ = allDivMain$$[i];
        console.log(divMain$$);
        if (divMain$$.getAttribute('data-function') == "fn_list") {
            divMain$$.style.display = "block";
        } else {
            divMain$$.style.display = "none";
        }
    }
}

const back$$ = document.querySelector('[data-function = "main_detail_container_back"]');
back$$.addEventListener('click', fnAtrasDetail);



